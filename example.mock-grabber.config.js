export const data = {
    drupalSiteUrl: 'https://YOUR_DRUPAL_URL',
    jsonApiEndpoint: 'jsonapi',
    mocks: [
        {'MOCKS_FILE_NAME': '/API/PATH', 'options':{'includes': ['field1']}},
        {'MOCKS_FILE_NAME2': '/API/PATH2', 'options':{'includes': ['field1'],'fields': ['field1']}},
        {'MOCKS_FILE_NAME2': '/API/PATH2'},
    ],
    extractSingleInstances: false
};