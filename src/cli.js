import execa from 'execa';
import Listr from 'listr';
import chalk from 'chalk';
import fs from 'fs';
import path from 'path';
import Observable from 'zen-observable';

let config = false;
if (fs.existsSync(`${process.cwd()}/mock-grabber.config.js`)) {
  config = require(`${process.cwd()}/mock-grabber.config.js`).data;
}

export async function cli(args) {

  if (!config) {
    console.log(chalk.red('Could not load config.') + chalk.bgGray('\nCreate a mock-grabber.config.js'));
    return;
  }
  console.log(chalk.green("let's download some mocks!"));

  await runTasks()
}

const runTasks = async () => {

  const tasks = new Listr([
    {
      title: 'Download Mocks',
      task: () => {
        return new Observable(observer => {
          downloadMocks(observer);
        });
      }
    },
  ]);

  await tasks.run();
}

async function createDir(dirName) {
  const createMockDirResult = await execa('mkdir', [dirName]);
  if (createMockDirResult.failed) {
    Promise.reject(new Error(`Failed to create ${dirName}!`));
  }
}

async function downloadApiJson(name, uri) {
  const downloadResult = await execa('wget', [uri, '-O', name]);

  if (downloadResult.failed) {
    Promise.reject(new Error('Failed to grab mocks!'));
  }
}

async function moveFiles(file, mockDir, name) {
  const moveFilesResult = await execa('mv', [file, `mocks/${mockDir}/${name}`]);
  if (moveFilesResult.failed) {
    Promise.reject(new Error('Failed to move mocks into directory'));
  }
  await makePrettyPrint(mockDir, name)
}

async function makePrettyPrint(mockDir, name) {
  const mockJsonPAth = path.join(__dirname, '..', `mocks/${mockDir}/${name}`)
  const mockJson = require(mockJsonPAth);
  fs.writeFileSync(mockJsonPAth, JSON.stringify(mockJson, null, 2));
}

async function getSingleInstances(mockDir, filename, name, query, observer) {
  var json = require(`${process.cwd()}/mocks/${mockDir}/${filename}`);
  if (Array.isArray(json.data)) {
    for (const vv of json.data) {
      if (!fs.existsSync(`mocks/${mockDir}/${name}`)) {
        await createDir(`mocks/${mockDir}/${name}`);
      }

      let uri = vv.links.self.href;
      if (uri.includes('?')) {
        query = query.replace('?', '&');
      }

      await downloadApiJson(vv.id, `${vv.links.self.href}${query}`);

      let newFileName = `${vv.attributes.title.replace(/\s|[()&".,]/g, '_').toLowerCase()}`;
      // Remove extra underscore characters.
      newFileName = newFileName.replace(/[_]+/g, '_');
      // Remove any trailing (_ ? . !)
      newFileName = newFileName.replace(/[ _?.!]*$/g, '');
      // Add the file extension back
      newFileName += '.json';

      observer.next(`${mockDir}/${name}/${newFileName}`);

      await moveFiles(vv.id, `${mockDir}/${name}`, newFileName);
    }
  }
}

function buildQueryString(v) {
  let query = '?';
  let queryParams = [];
  if (v.options) {
    for (const option of Object.keys(v.options)) {
      queryParams.push(`${option}=${v.options[option].join(',').replace(' ', '')}`);
    }
    query += queryParams.join('&');
  }
  return query;
}

const downloadMocks = async (observer) => {
  if (!fs.existsSync(`mocks`)) {
    await createDir(`mocks`);
  }

  for (const v of config.mocks) {
    let name = Object.keys(v)[0];
    let filename = `${name}.json`;
    let uri = Object.entries(v)[0][1]
    let mockDir = uri.split("/")[1];

    if (!fs.existsSync(`mocks/${mockDir}`)) {
      await createDir(`mocks/${mockDir}`);
    }
    let query = buildQueryString(v);

    observer.next(`${mockDir}/${filename}`);
    await downloadApiJson(name, `${config.drupalSiteUrl}/${config.jsonApiEndpoint}${uri}${query}`);
    await moveFiles(name, mockDir, filename);
    if (config.extractSingleInstances) {
      await getSingleInstances(mockDir, filename, name, query, observer);
    }

  }
  observer.complete();
  return;
}