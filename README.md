# Drupal JSON:API Mocks Extractor

> Pulls JSONAPI mocks from specified drupal site.

## Introduction

In order to use the script you must create a `mock-grabber.config.js` file.

```javascript
export const data = {
    drupalSiteUrl: 'https://YOUR_DRUPAL_URL',
    jsonApiEndpoint: 'jsonapi',
    mocks: [
        {'MOCKS_FILE_NAME': '/API/PATH', 'options':{'includes': ['field1']}},
        {'MOCKS_FILE_NAME2': '/API/PATH2', 'options':{'includes': ['field1'],'fields': ['field1']}},
        {'MOCKS_FILE_NAME2': '/API/PATH2'},
    ],
    extractSingleInstances: false
};
```
- drupalSiteUrl: is the url where your drupal site is hosted
- jsonApiEndpoint: is the endpoint for your jsonapi (default is https://SITEURL/jsonapi)
- mocks: (array) of objects keyed by name with a path value
    - name: will become the file name for the `mock.json` of a specified endpoint.
    - path: is the endpoint path (after `jsonApiEndpoint`)
        - ie: `https://YOUR_DRUPAL_URL/jsonapi/commerce_product/PRODUCT_TYPE`
        - the path would be `/commerce_product/PRODUCT_TYPE`
    - options (object) (optional) object key by query parameter name and array of fields.
- extractSingleInstance: (boolean) if true, and the mock data pulled is an array. It will also pull the individual
  mock files and name them based on the `data.attributes.name`.
  
## How to use

- `npm install @mdupree/drupal-jsonapi-mocks-extractor`

OR

- `yarn add @mdupree/drupal-jsonapi-mocks-extractor`

- create a `mock-grabber.config.js`

- run `yarn drupal-jsonapi-mocks-extractor`